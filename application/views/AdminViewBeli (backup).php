<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Given Kopi - ADMIN</title>

    <!-- Fontfaces CSS-->
    <link href="assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

    <!-- Main CSS-->
    <link href="assets/admin/css/theme.css" rel="stylesheet" media="all">

    <style>
        .container {
            width: 100%;
        }

        #btn-oke {
            position: relative;
            left: 90%;
        }

        @media (max-width: 2000px) {
            .container {
                width: 45%;
            }
        }

        @media (max-width: 1080px) {
            .container {
                width: 70%;
            }
        }

        @media (max-width: 1536px) {
            .container {
                width: 60%;
            }

            #btn-oke {
                position: relative;
                left: 90%;
            }
        }

        @media (max-width: 720px) {
            .container {
                width: 100%;
            }

            #btn-oke {
                position: relative;
                left: 85%;
            }
        }
    </style>

</head>

<body class="animsition">


    <div class="page-wrapper">

        <?php
        $this->load->view("components/admin/header")
        ?>

        <div class="container" style="margin-top: 50px;">
            <form id="adminBeliKopi">
                <div class="custom-file" style="margin-bottom: 20px;">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                <div class="form-row" style="margin-bottom: 20px;">
                    <div class="col">
                        <input type="text" name="jenis_kopi" class="form-control" placeholder="Jenis Kopi">
                    </div>
                </div>
                <div class="form-row" style="margin-bottom: 20px;">
                    <div class="col">
                        <input type="number" name="harga" class="form-control" placeholder="Harga (per kg)">
                    </div>
                </div>
                <div class="form-row" style="margin-bottom: 20px;">
                    <div class="col">
                        <input type="number" name="bobot_butuh" class="form-control" placeholder="Bobot yang dibutuhkan (kg)">
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="kualitas" placeholder="Kualitas Kopi" rows="3"></textarea>
                </div>
                <div id="btn-oke">
                    <button type="submit" class="btn btn-success">Oke</button>
                    <!-- <button type="button" class="btn btn-danger">Batal</button> -->
                </div>
            </form>
        </div>


    </div>


    <!-- Jquery JS-->
    <script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="assets/admin/vendor/slick/slick.min.js"></script>
    <script src="assets/admin/vendor/wow/wow.min.js"></script>
    <script src="assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="assets/admin/vendor/counter-up/jquery.counterup.min.js"></script>
    <script src="assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="assets/admin/vendor/select2/select2.min.js"></script>
    <script src="assets/admin/js/main.js"></script>

    <!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 5000
        });

        $('form[id=adminBeliKopi').submit(function(e) {
            e.preventDefault();

            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove(); // remove the error text
            $('.alert-success').remove();

            var formData = {};

            var datas = $(this).serializeArray();
            datas.map(function(item, index, array) {
                formData[item.name] = item.value;
            });

            // alert("Submitted");
            // alert(JSON.stringify(formData));

            var formUrl = "<?= base_url("tambahKopiMentah") ?>";

            // process the form
            $.ajax({
                    type: 'POST',
                    url: formUrl,
                    data: formData, // data object
                    dataType: 'json', // what type of data do we expect back from the serverss
                    error: function(data) {
                        alert("AJAX ERROR")
                        alert(JSON.stringify(data));
                    }
                })

                // using the done promise callback
                .done(function(data) {

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        Toast.fire({
                            type: 'error',
                            title: data.message
                        });

                    } else {

                        // ALL GOOD! just show the success message!
                        // $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
                        Toast.fire({
                            type: 'success',
                            title: data.message
                        });

                        setTimeout(function() {
                            window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000); //will call the function after 2 secs.

                    }
                });
        });
    </script>

    <!-- JS Add Images -->
    <script type="text/javascript">
        $(document).ready(function() {

            $(".btn-success").click(function() {
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click", ".btn-danger", function() {
                $(this).parents(".control-group").remove();
            });

        });
    </script>

    <!-- JS Map -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
    <script src="{{ asset('assets/js/mapInput.js') }}"></script> -->



</body>

</html>
