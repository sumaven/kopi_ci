﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<!-- <link href="assets/bootstrap/css/bootstrap_lelang.min.css" rel="stylesheet"> -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Fontfaces CSS-->
	<link href="assets/admin/css/font-face.css" rel="stylesheet" media="all">
	<link href="assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
	<link href="assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
	<link href="assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

	<!-- Datetimepicker -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.datetimepicker.css">

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-12">

			<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
				<button class="btn btn-success" style="width: 100%;">
					Lelang
				</button>
			</div>

			<!-- Row start -->
			<div class="row">
				<?php foreach ($rows as $row) : ?>
					<div class="col-lg-4 col-md-4 mb-4">
						<div class="card h-100">
							<!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
							<div class="card-body">
								<img src="<?= $row->image_location ?>?<?= filemtime($row->image_location) ?>" height="200"
									 style="width: 100%; margin: 0 auto;"/>
								<h4 class="card-title" style="margin-top: 20px;">
									<a href="#"><?= $row->jenis_kopi ?></a>
								</h4>
								<table>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Bobot lelang</td>
										<td>: <?= $row->bobot_lelang ?></td>
									</tr>
									<tr>
										<td>Kualitas</td>
										<td>: <?= $row->kualitas ?></td>
									</tr>
									<tr>
										<td>Harga bid awal</td>
										<td>: <?= $row->harga_bid_awal ?></td>
									</tr>
									<tr>
										<td>Harga beli sekarang</td>
										<td>: <?= $row->harga_beli_sekarang ?></td>
									</tr>
									<tr>
										<td>Kelipatan bid</td>
										<td>: <?= $row->kelipatan_bid ?></td>
									</tr>
									<tr>
										<td>Proses pasca panen</td>
										<td>: <?= $row->proses_pasca_panen ?></td>
									</tr>
									<tr>
										<td>Ketinggian lahan</td>
										<td>: <?= $row->ketinggian_lahan ?></td>
									</tr>
									<tr>
										<td>Waktu mulai</td>
										<td>: <?= $row->waktu_mulai ?></td>
									</tr>
									<tr>
										<td>Waktu berakhir</td>
										<td>: <?= $row->waktu_berakhir ?></td>
									</tr>

									<?php
									$date = new DateTime();
									$current_datetime = $date->format("Y-m-d H:i:s");


									$datetime = new DateTime();
									$datetime2 = new DateTime($row->waktu_mulai);
									$diff = $datetime2->getTimestamp() - $datetime->getTimestamp();
									?>

									<?php if ($diff > 0) : ?>
										<tr>
											<td colspan="2">Lelang akan mulai dalam waktu</td>
										</tr>
										<tr>
											<td colspan="2">
												<span id="countdown-<?= $row->id_lelang ?>" class="timer"></span>
											</td>
										</tr>
									<?php endif; ?>
								</table>
							</div>

							<script>
								var upgradeTime_<?= $row->id_lelang ?> = <?= $diff ?>;
								var seconds_<?= $row->id_lelang ?> = upgradeTime_<?= $row->id_lelang ?>;

								function timer_<?= $row->id_lelang ?>() {
									var days = Math.floor(seconds_<?= $row->id_lelang ?> / 24 / 60 / 60);
									var hoursLeft = Math.floor((seconds_<?= $row->id_lelang ?>) - (days * 86400));
									var hours = Math.floor(hoursLeft / 3600);
									var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
									var minutes = Math.floor(minutesLeft / 60);
									var remainingSeconds = seconds_<?= $row->id_lelang ?> % 60;

									function pad(n) {
										return (n < 10 ? "0" + n : n);
									}

									document.getElementById('countdown-<?= $row->id_lelang ?>').innerHTML = pad(days) + " hari " + " " + pad(hours) + "jam " + pad(minutes) + "menit " + pad(remainingSeconds) + "detik";
									if (seconds_<?= $row->id_lelang ?> == 0) {
										clearInterval(countdownTimer_<?= $row->id_lelang ?>);
										document.getElementById('countdown-<?= $row->id_lelang ?>').innerHTML = "Completed";
									} else {
										seconds_<?= $row->id_lelang ?>--;
									}
								}

								var countdownTimer_<?= $row->id_lelang ?> = setInterval('timer_<?= $row->id_lelang ?>()', 1000);
							</script>

							<?php
							$keranjang_query = $this->lelang_model->getKeranjangByLelangId($row->id_lelang);
							$keranjang_item = $keranjang_query->row();
							$is_disabled = $keranjang_item != null || ($current_datetime > $row->waktu_berakhir) || ($current_datetime < $row->waktu_mulai);
							?>

							<div class="card-footer">
								<button <?= $is_disabled ? "disabled" : "" ?> class="btn btn-info"
									onclick="document.getElementById('tambahEditBidModalForm-<?= $row->id_lelang ?>').style.display='block'"
									style="width:auto;">Pilih
								</button>
							</div>
						</div>
					</div>

					<!-- modal delete -->
					<div id="tambahEditBidModalForm-<?= $row->id_lelang ?>" class="modal">
						<form id="tambahEditBid" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Atur bid</h5>
										<button type="button" class="close"
												onclick="document.getElementById('tambahEditBidModalForm-<?= $row->id_lelang ?>').style.display='none'"
												data-dismiss="tambahBidModalForm" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<?php
										$data_pass = array(
												"id_lelang" => $row->id_lelang,
												"id_pembeli" => $id_pembeli,
										);

										$query = $this->db->get_where('lelang_bid', $data_pass, 1, 0);
										$bid = $query->row();
										?>

										<table>
											<tr>
												<td style="width: 80%">Harga bid awal</td>
												<td>: <?= $row->harga_bid_awal ?></td>
											</tr>
											<tr>
												<td>Kelipatan bid</td>
												<td>: <?= $row->kelipatan_bid ?></td>
											</tr>
											<tr>
												<td>Harga beli sekarang</td>
												<td>: <?= $row->harga_beli_sekarang ?></td>
											</tr>
										</table>

										</br>

										<?php
										$bid_query = $this->lelang_model->getMaxBid($row->id_lelang);
										$bid_item = $bid_query->row();
										$bid_paling_tinggi = $bid_item->bid;
										?>

										<div class="form-group">
											<label for="bid">BID tertinggi : <?= $bid_paling_tinggi ?></label>
											<input type="number" name="bid" class="form-control" id="bid" placeholder="Masukkan jumlah bid"
												   value="<?= (!empty($bid)) ? $bid->bid : "" ?>">
										</div>
										<input type="hidden" name="id_lelang" value="<?= $row->id_lelang ?>"/>
										<input type="hidden" name="id_pembeli" value="<?= $id_pembeli ?>"/>
									</div>

									<?php
									$bid_query = $this->lelang_model->getMaxBid($row->id_lelang);
									$bid_item = $bid_query->row();
									$is_disabled = $bid_item->bid > $row->harga_beli_sekarang;
									?>

									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('tambahEditBidModalForm-<?= $row->id_lelang ?>').style.display='none'"
												class="btn btn-success">Oke
										</button>
										<button <?= $is_disabled ? "disabled" : "" ?> id="beli_sekarang_btn" data-idlelang="<?= $row->id_lelang ?>"
											data-idpembeli="<?= $id_pembeli ?>"
											data-biayapenawaran="<?= $row->harga_beli_sekarang ?>"
											onclick="document.getElementById('tambahEditBidModalForm-<?= $row->id_lelang ?>').style.display='none'"
											class="btn btn-success">Beli Sekarang
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				<?php endforeach; ?>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->

<?php
$this->load->view("components/footer")
?>

<!-- Jquery JS-->
<script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="assets/admin/vendor/slick/slick.min.js"></script>
<script src="assets/admin/vendor/wow/wow.min.js"></script>
<script src="assets/admin/vendor/animsition/animsition.min.js"></script>
<script src="assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="assets/admin/vendor/counter-up/jquery.counterup.min.js"></script>
<script src="assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="assets/admin/vendor/select2/select2.min.js"></script>
<script src="assets/admin/js/main.js"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<!-- Datetimepicker -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.datetimepicker.js"></script>


<script>
	$('.form_datetime').datetimepicker({
		//language:  'fr',
		weekStart: 1,
		todayBtn: 1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		showMeridian: 1
	});

	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

<script>
	// tambahLelang

	$('form[id=tambahEditBid').submit(function (e) {
		e.preventDefault();

		var id_lelang = $(this).attr("data-idlelang");
		var id_pembeli = $(this).attr("data-idpembeli");

		if (id_lelang != undefined && id_pembeli != undefined) {
			return;
		}

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formUrl = "<?= base_url("tambahEditBid") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: new FormData(this), //penggunaan FormData
			dataType: 'json', // what type of data do we expect back from the serverss
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});

	$(document).ready(function (e) {
		$("button[id=beli_sekarang_btn]").click(function () {
			var id_lelang = $(this).attr("data-idlelang");
			var id_pembeli = $(this).attr("data-idpembeli");
			var biaya_penawaran = $(this).attr("data-biayapenawaran");

			if (id_lelang != undefined && id_pembeli != undefined) {

				var formData = {};
				formData["biaya_penawaran"] = biaya_penawaran;
				formData["id_bid"] = -1;
				formData["id_lelang"] = id_lelang;
				formData["id_pembeli"] = id_pembeli;

				// process the form
				$.ajax({
					type: 'POST',
					url: "<?= base_url("beliSekarang") ?>",
					data: formData, // data object
					dataType: 'json', // what type of data do we expect back from the serverss
					error: function (data) {
						alert("AJAX ERROR")
						alert(JSON.stringify(data));
					}
				}).done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});

			}
		});
	});
</script>

</body>

</html>
