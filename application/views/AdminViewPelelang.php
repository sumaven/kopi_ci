<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GIVEN KOPI - JUAL & BELI KOPI</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap_lelang.min.css" rel="stylesheet"> -->
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fontfaces CSS-->
    <link href="<?= base_url() ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?= base_url() ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?= base_url() ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?= base_url() ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/shop-homepage.css" rel="stylesheet">

    <!-- Main CSS-->
    <link href="<?= base_url() ?>assets/admin/css/theme.css" rel="stylesheet" media="all">

    <!-- Custom styles -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

    <!-- Datetimepicker -->
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.datetimepicker.css">

</head>

<body>

    <div id="main-wrapper">

        <!-- Navigation Start -->
        <?php
        $this->load->view("components/admin/header")
        ?>
        <!-- Navigation End -->

        <!-- Page Content -->
        <div class="container" style="padding-top: 80px;">

            <div class="row">

                <!-- /.col-lg-3 -->

                <div class="col-lg-12">

                    <div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
                        <button class="btn btn-primary" style="width: 100%;" onclick="document.getElementById('tambahLelangModal').style.display='block'">
                            Daftar Pelelang
                        </button>
                    </div>


                    <!-- Row start -->
                    <div class="row">

                        <?php foreach ($rows as $row) : ?>

                            <div class="col-lg-4 col-md-4 mb-4">
                                <div class="card h-100">
                                    <!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
                                    <div class="card-body">
                                        <h4 class="card-title" style="margin-top: 20px;">
                                            <a href="#"><?= $row->nama ?></a>
                                        </h4>
                                        <p class="card-text">Tawaran : <?= $row->bid ?></p>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-success" onclick="document.getElementById('tambahKeranjangFormModal<?= $row->id_pembeli ?>').style.display='block'" style="width:auto;">Pilih pemenang</button>
                                    </div>
                                </div>
                            </div>

                            <!-- modal delete -->
                            <div id="tambahKeranjangFormModal<?= $row->id_pembeli ?>" class="modal">
                                <form id="tambahKeranjang" method="post">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Pilih pemenang</h5>
                                                <button type="button" class="close" onclick="document.getElementById('tambahKeranjangFormModal<?= $row->id_pembeli ?>').style.display='none'" data-dismiss="hapusModalForm" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <p>Apakah Anda yakin ?</p>
                                                <input type="hidden" name="id_lelang" value="<?= $row->id_lelang ?>" />
                                                <input type="hidden" name="id_pembeli" value="<?= $row->id_pembeli ?>" />
                                                <input type="hidden" name="id_bid" value="<?= $row->id_bid ?>" />
                                                <input type="hidden" name="biaya_penawaran" value="<?= $row->bid ?>" />

                                            </div>
                                            <div class="modal-footer">
                                                <!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
                                                <button type="submit" onclick="document.getElementById('tambahKeranjangFormModal<?= $row->id_pembeli ?>').style.display='none'" class="btn btn-success">Ya</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                        <?php endforeach; ?>

                    </div>
                    <!-- /.row -->




                </div>
                <!-- /.col-lg-9 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->




        <?php
        $this->load->view("components/footer")
        ?>

    </div>

    <!-- Bootstrap core JavaScript -->
    <!-- <script src="<?= base_url() ?>assets/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.bundle.min.js"></script> -->

    <!-- Jquery JS-->
    <script src="<?= base_url() ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?= base_url() ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?= base_url() ?>assets/admin/vendor/slick/slick.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/counter-up/jquery.counterup.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/vendor/select2/select2.min.js"></script>
    <script src="<?= base_url() ?>assets/admin/js/main.js"></script>

    <!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

    <!-- Datetimepicker -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.datetimepicker.js"></script>


    <script>
        $('.form_datetime').datetimepicker({
            //language:  'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });

        const Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 5000
        });
    </script>


    <script>
        // tambahKeranjang

        $('form[id=tambahKeranjang').submit(function(e) {
            e.preventDefault();

            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove(); // remove the error text
            $('.alert-success').remove();

            var formData = {};

            var datas = $(this).serializeArray();
            datas.map(function(item, index, array) {
                formData[item.name] = item.value;
            });

            // alert("Submitted");
            alert(JSON.stringify(formData));

            var formUrl = "<?= base_url("AdminController/tambahKeranjang") ?>";

            // process the form
            $.ajax({
                    type: 'POST',
                    url: formUrl,
                    data: formData, // data object
                    dataType: 'json', // what type of data do we expect back from the serverss
                    error: function(data) {
                        alert("AJAX ERROR")
                        alert(JSON.stringify(data));
                    }
                })

                // using the done promise callback
                .done(function(data) {

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        Toast.fire({
                            type: 'error',
                            title: data.message
                        });

                    } else {

                        // ALL GOOD! just show the success message!
                        // $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
                        Toast.fire({
                            type: 'success',
                            title: data.message
                        });

                        setTimeout(function() {
                            window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
                        }, 1000); //will call the function after 2 secs.

                    }
                });
        });
    </script>


</body>

</html>
