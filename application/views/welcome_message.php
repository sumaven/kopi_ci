<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Bootstrap Theme Simply Me</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  body {
      font: 15px Montserrat, sans-serif;
      line-height: 1.8;
      color: #2e6650;
  }
  p {font-size: 15px;}
  .margin {margin-bottom: 10px;}
  .bg-1 { 
      background-color: #f7ba7b; /* Light Orange */
      color: #2e6650;
  }
  .bg-2 { 
      background-color: #f7ba7b; /* Dark Blue */
      color: #00798c;
  }
  .btn-default {
      background-color: #00798c;
      color: #ffffff;
  }
  .btn-primary {
      background-color: #ffdfbf;
      color: #2e6650;
  }
  .container-fluid {
      padding-top: 70px;
      padding-bottom: 70px;
  }
  .form-control {
  background-color: #ffdfbf;
  color: #2e6650;
  }
  .navbar {
      background-color: #f5a44d;
      padding-top: 30px;
      padding-bottom: 15px;
      border: 0;
      border-radius: 0;
      margin-bottom: 0;
      font-size: 12px;
      letter-spacing: 5px;
  }
  .navbar-nav  li a:hover {
      color: #f7ba7b !important;
  }
  </style>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="assets/img/header-logo.png" class="img-responsive" alt=""></a>
    </div>
  </div>
</nav>

<!-- First Container -->
<div class="container-fluid bg-1 text-center">
  <h3 class="margin">Welcome</h3>
  <form class="form-horizontal" action="/action_page.php">
    <div class="form-group">
      <label class="control-label col-sm-5" for="code">Kode Institusi:</label>
      <div class="col-sm-3">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Institution Code
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="#">UI0001 - UNIVERSITAS INDONESIA</a></li>
            <li><a href="#">UB0002 - UNIVERSITAS BRAWIJAYA</a></li>
            <li><a href="#">UGM003 - UNIVERSITAS GAJAH MADA</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-5" for="username">Username:</label>
      <div class="col-sm-3">
        <input type="email" class="form-control" id="username" placeholder="Enter NIM / NIP">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-5" for="pwd">Password:</label>
      <div class="col-sm-3"> 
        <input type="password" class="form-control" id="pwd" placeholder="Enter DOB">
      </div>
    </div>
    <div class="form-group"> 
      <div class="col-sm-offset-4 col-sm-4">
        <div class="checkbox">
          <label><input type="checkbox"> Remember me</label>
        </div>
      </div>
    </div>
    <div class="form-group"> 
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Login</button>
      </div>
    </div>
  </form>
</div>
<!-- Footer -->
<footer class="container-fluid bg-2 text-center">
  <p>2018 PT. BANK NEGARA INDONESIA (PERSERO) Tbk.<br>All Rights Reserved.</p> 
</footer>

</body>
</html>
