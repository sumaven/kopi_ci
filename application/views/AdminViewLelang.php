<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container" style="margin-top: 30px; min-height: 500px">
	<div class="row">
		<div class="col-lg-12 mt-5">
			<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
				<button class="btn btn-success" style="width: 100%;" onclick="document.getElementById('tambahLelangModal').style.display='block'">
					Tambah Lelang
				</button>
			</div>

			<!-- Row start -->
			<div class="row">
				<?php foreach ($rows as $row) : ?>
					<div class="col-lg-4 col-md-4 mb-4">
						<div class="card h-100">
							<!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
							<div class="card-body">
								<img src="<?= $row->image_location ?>?<?= filemtime($row->image_location) ?>" style="width: 100%; margin: 0 auto;"/>
								<h4 class="card-title" style="margin-top: 20px;">
									<a href="#"><?= $row->jenis_kopi ?></a>
								</h4>
								<p class="card-text">Bobot lelang : <?= $row->bobot_lelang ?></p>
							</div>
							<div class="card-footer">
								<a class="btn btn-primary" href="<?= base_url("lelang") . "/" . $row->id_lelang ?>">Lihat daftar penawar</a>
								<button class="btn btn-danger"
										onclick="document.getElementById('hapusModalForm-<?= $row->id_lelang ?>').style.display='block'"
										style="width:auto;">Hapus
								</button>
							</div>
						</div>
					</div>

					<!-- modal delete -->
					<div id="hapusModalForm-<?= $row->id_lelang ?>" class="modal">
						<form id="hapusLelang" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
										<button type="button" class="close"
												onclick="document.getElementById('hapusModalForm-<?= $row->id_lelang ?>').style.display='none'"
												data-dismiss="hapusModalForm" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<p>Yakin ingin menghapus ?</p>
										<input type="hidden" name="id_lelang" value="<?= $row->id_lelang ?>"/>

									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('hapusModalForm-<?= $row->id_lelang ?>').style.display='none'"
												class="btn btn-danger">Ya (Hapus)
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				<?php endforeach; ?>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->

<!-- modal form -->
<div id="tambahLelangModal" class="modal">
	<form id="tambahLelang" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
					<button type="button" class="close" onclick="document.getElementById('tambahLelangModal').style.display='none'"
							data-dismiss="modalform" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="gambarKopi">Gambar Kopi</label>
						<input type="file" name="gambar_kopi" class="form-control-file" id="gambarKopi">
					</div>

					<div class="form-group">
						<label for="jenis_kopi">Jenis Kopi</label>
						<input type="text" name="jenis_kopi" class="form-control" id="jenis_kopi" placeholder="Jenis Kopi">
					</div>

					<div class="form-group">
						<label for="bobot_lelang">Bobot Lelang</label>
						<input type="number" name="bobot_lelang" class="form-control" id="bobot_lelang" placeholder="Masukkan bobot lelang">
					</div>

					<?php
					$date = new DateTime();
					$now = $date->format('Y-m-d') . 'T' . $date->format('H:i:s') . 'Z';
					?>

					<div class="form-group">
						<label for="waktu_mulai">Waktu mulai</label>
						<div class="input-group date form_datetime" data-date="<?= $now ?>" data-date-format="dd MM yyyy - HH:ii p"
							 data-link-field="waktu_mulai">
							<input class="form-control" size="16" type="text" value="" readonly>
							<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
						</div>

						<input type="hidden" name="waktu_mulai" id="waktu_mulai" value=""/>
					</div>

					<div class="form-group">
						<label for="waktu_berakhir">Waktu berakhir</label>
						<div class="input-group date form_datetime" data-date="<?= $now ?>" data-date-format="dd MM yyyy - HH:ii p"
							 data-link-field="waktu_berakhir">
							<input class="form-control" size="16" type="text" value="" readonly>
							<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
						</div>

						<input type="hidden" name="waktu_berakhir" id="waktu_berakhir" value=""/>
					</div>

					<div class="form-group">
						<label for="kualitas">Kualitas</label>
						<textarea class="form-control" id="kualitas" name="kualitas" placeholder="Kualitas Kopi" rows="3"></textarea>
					</div>

					<div class="form-group">
						<label for="harga_bid_awal">Harga Bid Awal</label>
						<input type="number" name="harga_bid_awal" class="form-control" id="harga_bid_awal" placeholder="Harga bid awal">
					</div>

					<div class="form-group">
						<label for="harga_beli_sekarang">Harga Beli Sekarang</label>
						<input type="number" name="harga_beli_sekarang" class="form-control" id="harga_beli_sekarang"
							   placeholder="Harga beli sekarang">
					</div>

					<div class="form-group">
						<label for="kelipatan_bid">Kelipatan Bid</label>
						<input type="number" name="kelipatan_bid" class="form-control" id="kelipatan_bid" placeholder="Kelipatan bid">
					</div>

					<div class="form-group">
						<label for="proses_pasca_panen">Proses Pasca Panen</label>
						<textarea class="form-control" id="proses_pasca_panen" name="proses_pasca_panen" placeholder="Kualitas Kopi"
								  rows="3"></textarea>
					</div>

					<div class="form-group">
						<label for="ketinggian_lahan">Ketinggian Lahan (meter)</label>
						<input type="number" name="ketinggian_lahan" class="form-control" id="ketinggian_lahan"
							   placeholder="Ketinggian lahan (meter)">
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" onclick="document.getElementById('tambahLelangModal').style.display='none'" class="cancelbtn">Cancel
					</button>
					<button type="submit" onclick="document.getElementById('tambahLelangModal').style.display='none'" class="btn btn-primary">Save
					</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/admin/js/main.js"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<!-- Datetimepicker -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.datetimepicker.js"></script>

<script>
	$('.form_datetime').datetimepicker({
		//language:  'fr',
		weekStart: 1,
		todayBtn: 1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		showMeridian: 1
	});

	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

<script>
	// tambahLelang

	$('form[id=tambahLelang').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formUrl = "<?= base_url("tambahLelang") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: new FormData(this), //penggunaan FormData
			dataType: 'json', // what type of data do we expect back from the serverss
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>


<script>
	// hapusLelang

	$('form[id=hapusLelang').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formData = {};

		var datas = $(this).serializeArray();
		datas.map(function (item, index, array) {
			formData[item.name] = item.value;
		});

		// alert("Submitted");
		// alert(JSON.stringify(formData));

		var formUrl = "<?= base_url("hapusLelang") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: formData, // data object
			dataType: 'json', // what type of data do we expect back from the serverss
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>


<script>
	$(document).ready(function () {
		$("a").click(function () {
			var id = $(this).attr('id');
			var id_transaksi_jual = $(this).attr('data-idtransaksijual');
			var status = $(this).attr("data-value");

			if (id_transaksi_jual != undefined) {

				var formData = {};
				formData["id_transaksi_jual"] = id_transaksi_jual;
				formData["status"] = status;

				// process the form
				$.ajax({
					type: 'POST',
					url: "<?= base_url("ubahStatus") ?>",
					data: formData, // data object
					dataType: 'json', // what type of data do we expect back from the serverss
					error: function (data) {
						alert("AJAX ERROR")
						alert(JSON.stringify(data));
					}
				}).done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});


			}

		});
	});
</script>

</body>

</html>
