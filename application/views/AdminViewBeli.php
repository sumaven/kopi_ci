<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container" style="margin-top: 30px; min-height: 500px">
	<div class="row">
		<div class="col-lg-12 mt-5">

			<div class="row" style="width: 100%; margin: 0 auto; padding: 0 35px 30px 35px;">
				<button class="btn btn-success" style="width: 100%;"
						onclick="document.getElementById('tambahKopiMentahModal').style.display='block'">
					Cari Kopi
				</button>
			</div>

			<div class="row">
				<?php foreach ($rows as $row) : ?>
					<div class="col-lg-4 col-md-4 mb-4">
						<div class="card h-100">
							<!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
							<div class="card-body">
								<img src="<?= $row->image_location ?>?<?= filemtime($row->image_location) ?>"
									 style="width: 100%; margin: 0 auto;"/>
								<h4 class="card-title" style="margin-top: 20px;">
									<a href="#"><?= $row->jenis_kopi ?></a>
								</h4>
								<p class="card-text">Bobot yang dibutuhkan : <?= $row->bobot_butuh ?></p>
								<p class="card-text" style='margin-bottom: -40px;'>Bobot sekarang : <?= $row->bobot ?></p>
							</div>
							<div class="card-footer">
								<button class="btn btn-primary"
										onclick="document.getElementById('editModalform-<?= $row->id_kopi_mentah ?>').style.display='block'"
										style="width:auto;">Edit
								</button>
								<button class="btn btn-danger"
										onclick="document.getElementById('hapusModalForm-<?= $row->id_kopi_mentah ?>').style.display='block'"
										style="width:auto;">Hapus
								</button>
							</div>
						</div>
					</div>

					<!-- modal delete -->
					<div id="hapusModalForm-<?= $row->id_kopi_mentah ?>" class="modal">
						<form id="hapusKopiMentah" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
										<button type="button" class="close"
												onclick="document.getElementById('hapusModalForm-<?= $row->id_kopi_mentah ?>').style.display='none'"
												data-dismiss="hapusModalForm" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<p>Yakin ingin menghapus ?</p>
										<input type="hidden" name="id_kopi_mentah" value="<?= $row->id_kopi_mentah ?>"/>

									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('hapusModalForm-<?= $row->id_kopi_mentah ?>').style.display='none'"
												class="btn btn-danger">Ya (Hapus)
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<!-- modal form -->
					<div id="editModalform-<?= $row->id_kopi_mentah ?>" class="modal">
						<form id="editKopiMentah" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
										<button type="button" class="close"
												onclick="document.getElementById('editModalform-<?= $row->id_kopi_mentah ?>').style.display='none'"
												data-dismiss="editModalform" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<div class="form-group">
											<label for="gambarKopi">Gambar Kopi</label>
											<input type="file" name="gambar_kopi" class="form-control-file" id="gambarKopi">
										</div>

										<div class="form-group">
											<label for="jenis_kopi">Jenis Kopi</label>
											<input type="text" name="jenis_kopi" class="form-control" id="jenis_kopi" placeholder="Jenis Kopi"
												   value="<?= $row->jenis_kopi ?>">
										</div>

										<div class="form-group">
											<label for="harga">Harga</label>
											<input type="number" name="harga" class="form-control" id="harga" placeholder="Harga (per kg)"
												   value="<?= $row->harga ?>">
										</div>

										<div class="form-group">
											<label for="bobot_butuh">Bobot Butuh</label>
											<input type="number" name="bobot_butuh" class="form-control" id="bobot_butuh"
												   placeholder="Masukkan bobot butuh" value="<?= $row->bobot_butuh ?>">
										</div>

										<div class="form-group">
											<label for="kualitas">Kualitas</label>
											<textarea class="form-control" id="kualitas" name="kualitas" placeholder="Kualitas Kopi"
													  rows="3"><?= $row->kualitas ?></textarea>
										</div>

										<input type="hidden" name="id_kopi_mentah" value="<?= $row->id_kopi_mentah ?>"/>

									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('editModalform-<?= $row->id_kopi_mentah ?>').style.display='none'"
												class="btn btn-primary">Save
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>

				<?php endforeach; ?>
			</div>
			<!-- /.row -->

			<!-- ROW -->
			<?php if (count($jual_kopi_belum_valid_rows) > 0) : ?>
				<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 0px 30px 0px;">
					<div class="btn btn-secondary" style="width: 100%;">
						Daftar penjualan kopi semua petani (Belum divalidasi)
					</div>
				</div>
			<?php endif; ?>

			<div class="row">
				<?php foreach ($jual_kopi_belum_valid_rows as $row) : ?>
					<div class="col-lg-4 col-md-4 mb-4">
						<div class="card h-100">
							<!-- <a href="#"><img class="card-img-top" src="assets/img/kopi.jpg" alt=""></a> -->
							<div class="card-body">
								<img src="<?= $row->image_location ?>" style="width: 100%; margin: 0 auto;"/>
								<h4 class="card-title" style="margin-top: 20px;">
									<a href="#"><?= $row->jenis_kopi ?></a>
								</h4>
								<p class="card-text">Nama Petani : <?= $row->nama ?></p>
								<p class="card-text">Jenis Kopi : <?= $row->jenis_kopi ?></p>
								<p class="card-text">Kualitas : <?= $row->kualitas ?></p>
								<p class="card-text">Tinggi Rata-Rata Pohon : <?= $row->tinggi_pohon_kopi ?></p>
								<p class="card-text">Bobot (kg) : <?= $row->bobot ?></p>
								<p class="card-text">Harga penawaran (per kg) : Rp<?= $row->harga ?></p>
								<p class="card-text">Harga Total (kg) : <?= $row->harga * $row->bobot ?></p>
							</div>
							<div class="card-footer">
								<div class="container">
									<div class="row">

										<!-- <div class="col-sm">
                                                    <button class="btn btn-primary" onclick="document.getElementById('modalform-<?= $row->id_kopi_mentah ?>').style.display='block'">Pilih</button>
                                                </div> -->

										<div class="col-sm">
											<div class="dropdown">
												<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
													Ubah Status (<?= $row->status ?>)
												</button>
												<div class="dropdown-menu">
													<a id="validasi" style="cursor: pointer" data-idtransaksijual="<?= $row->id_transaksi_jual ?>"
													   data-value="sudah-divalidasi" class="dropdown-item">Validasi</a>
													<a id="batal-divalidasi" style="cursor: pointer"
													   data-idtransaksijual="<?= $row->id_transaksi_jual ?>" data-value="belum-divalidasi"
													   class="dropdown-item">Batal Validasi</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->

<!-- modal form -->
<div id="tambahKopiMentahModal" class="modal">
	<form id="tambahKopiMentah" method="post">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Jual Kopi</h5>
					<button type="button" class="close" onclick="document.getElementById('tambahKopiMentahModal').style.display='none'"
							data-dismiss="modalform" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="gambarKopi">Gambar Kopi</label>
						<input type="file" name="gambar_kopi" class="form-control-file" id="gambarKopi">
					</div>

					<div class="form-group">
						<label for="jenis_kopi">Jenis Kopi</label>
						<input type="text" name="jenis_kopi" class="form-control" id="jenis_kopi" placeholder="Jenis Kopi">
					</div>

					<div class="form-group">
						<label for="harga">Harga</label>
						<input type="number" name="harga" class="form-control" id="harga" placeholder="Harga (per kg)">
					</div>

					<div class="form-group">
						<label for="bobot_butuh">Bobot butuh</label>
						<input type="number" name="bobot_butuh" class="form-control" id="bobot_butuh" placeholder="Masukkan bobot butuh">
					</div>

					<div class="form-group">
						<label for="kualitas">Kualitas</label>
						<textarea class="form-control" id="kualitas" name="kualitas" placeholder="Kualitas Kopi" rows="3"></textarea>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" onclick="document.getElementById('tambahKopiMentahModal').style.display='none'" class="cancelbtn">
						Cancel
					</button>
					<button type="submit" onclick="document.getElementById('tambahKopiMentahModal').style.display='none'" class="btn btn-primary">
						Save
					</button>
				</div>
			</div>
		</div>
	</form>
</div>

<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/admin/js/main.js"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});

	$('form[id=editKopiMentah').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formUrl = "<?= base_url("editKopiMentahAdmin") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: new FormData(this), //penggunaan FormData
			dataType: 'json', // what type of data do we expect back from the serverss
			processData: false,
			contentType: false,
			cache: false,
			async: false, // what type of data do we expect back from the serverss
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>

<script>
	// tambahKopiMentah

	$('form[id=tambahKopiMentah').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formUrl = "<?= base_url("tambahKopiMentah") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: new FormData(this), //penggunaan FormData
			dataType: 'json', // what type of data do we expect back from the serverss
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>


<script>
	// hapusKopiMentah

	$('form[id=hapusKopiMentah').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formData = {};

		var datas = $(this).serializeArray();
		datas.map(function (item, index, array) {
			formData[item.name] = item.value;
		});

		// alert("Submitted");
		// alert(JSON.stringify(formData));

		var formUrl = "<?= base_url("hapusKopiMentahAdmin") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: formData, // data object
			dataType: 'json', // what type of data do we expect back from the serverss
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>

<script>
	$(document).ready(function () {
		$("a").click(function () {
			var id = $(this).attr('id');
			var id_transaksi_jual = $(this).attr('data-idtransaksijual');
			var status = $(this).attr("data-value");

			if (id_transaksi_jual != undefined) {

				var formData = {};
				formData["id_transaksi_jual"] = id_transaksi_jual;
				formData["status"] = status;

				// process the form
				$.ajax({
					type: 'POST',
					url: "<?= base_url("ubahStatus") ?>",
					data: formData, // data object
					dataType: 'json', // what type of data do we expect back from the serverss
					error: function (data) {
						alert("AJAX ERROR")
						alert(JSON.stringify(data));
					}
				}).done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});


			}

		});
	});
</script>

</body>

</html>
