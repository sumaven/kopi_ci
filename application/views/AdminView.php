<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container" style="margin-top: 30px; min-height: 500px">
	<div class="row">
		<div class="col-lg-12 mt-5">
			<div class="row">
				<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
					<button class="btn btn-secondary" style="width: 100%;"
							onclick="document.getElementById('tambahKopiMentahModal').style.display='block'">
						Pembukuan
					</button>
				</div>

			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
</div>

<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<!-- Jquery JS-->
<script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

</body>

</html>
