﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
	<!-- Toastr -->
	<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container">
	<div class="row">
		<!-- /.col-lg-3 -->
		<div class="col-lg-12 mt-4">
			<div class="row">
				<div class="row" style="width: 100%; margin: 0 auto; padding: 0px 35px 30px 35px;">
					<button class="btn btn-secondary" style="width: 100%;"
							onclick="document.getElementById('tambahKopiMentahModal').style.display='block'">
						Pembayaran
					</button>
				</div>

				<?php foreach ($rows as $row) : ?>
					<div class="col-lg-12 col-md-12 mb-4">
						<div class="card h-100">
							<div class="card-body">
								<div class="row">
									<div class="col-lg-4 col-md-4" align="center">
										<a href="" target="_blank">
											<img style="max-height: 150px; width: auto;" class="card-img-top"
												 src="<?= base_url($row->image_location) ?>" alt="">
										</a>
										</br></br>
										<a href="" target="_blank">
											Bukti Pembayaran :
											<img style="max-height: 150px; width: auto;" class="card-img-top"
												 src="<?= base_url($row->bukti_image_upload) ?>" alt="">
										</a>
									</div>
									<div class="col-lg-8 col-md-8">
										<h4 class="card-title">
											<a href="#"><?= $row->jenis_kopi ?></a>
										</h4>
										<p class="card-text">Bobot : <?= $row->bobot ?></p>
									</div>
								</div>
							</div>

							<div align="right" style="margin-bottom: 30px; margin-right: 30px;">
								<div class="dropdown">
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										Verifikasi
									</button>
									<div class="dropdown-menu">
										<a id="validasi" data-idpembayaran="<?= $row->id_pembayaran ?>" data-idpembeli="<?= $row->id_pembeli ?>"
										   data-idjual=<?= $row->id_jual ?> data-status="valid" class="dropdown-item">Validasi</a>
										<a id="batalkan" data-idpembayaran="<?= $row->id_pembayaran ?>" data-idpembeli="<?= $row->id_pembeli ?>"
										   data-idjual=<?= $row->id_jual ?> data-status="batal" class="dropdown-item">Batalkan</a>
									</div>
								</div>
							</div>
						</div>
					</div>


				<?php endforeach; ?>
			</div>
			<!-- /.col-lg-12 -->
		</div>
	</div>
</div>

<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<!-- Jquery JS-->
<script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>


<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

<script>
	$(document).ready(function () {

		$("a").click(function () {
			var id_pembayaran = $(this).attr('data-idpembayaran');
			var id_pembeli = $(this).attr('data-idpembeli');
			var id_jual = $(this).attr('data-idjual');
			var status = $(this).attr("data-status");

			if (id_pembayaran != undefined) {

				var formData = {};
				formData["id_pembayaran"] = id_pembayaran;
				formData["id_pembeli"] = id_pembeli;
				formData["id_jual"] = id_jual;
				formData["status"] = status;

				// process the form
				$.ajax({
					type: 'POST',
					url: "<?= base_url("validasi") ?>",
					data: formData, // data object
					dataType: 'json', // what type of data do we expect back from the serverss
					error: function (data) {
						alert("AJAX ERROR")
						alert(JSON.stringify(data));
					}
				}).done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});


			}

		});


	});
</script>

</body>

</html>
