﻿<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>GIVEN KOPI - JUAL & BELI KOPI</title>

	<!-- Bootstrap core CSS -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="assets/css/shop-homepage.css" rel="stylesheet">

	<!-- Custom styles -->
	<link href="assets/css/style.css" rel="stylesheet">

	<!-- SweetAlert2 -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/sweetalert2/sweetalert2.min.css">
	<!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url("assets") ?>/plugins/toastr/toastr.min.css">

</head>

<body>

<!-- Navigation Start -->
<?php
$this->load->view("components/member_header")
?>
<!-- Navigation End -->

<!-- Page Content -->
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-12">
			<div class="row">

				<?php foreach ($rows as $row) : ?>
					<div class="col-lg-4 col-md-4 mb-4">
						<div class="card h-100">
							<a href="#"><img class="card-img-top" src="<?= base_url($row->image_location) ?>?<?= filemtime($row->image_location) ?>"
											 alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#"><?= $row->jenis_kopi ?></a>
								</h4>
								<p class="card-text">
								<table>
									<tr>
										<td>Harga</td>
										<td>: <?= $row->harga ?></td>
									</tr>
									<tr>
										<td>Bobot Stok</td>
										<td>: <?= $row->bobot_stok ?></td>
									</tr>
									<tr>
										<td>Kualitas</td>
										<td>: <?= $row->kualitas ?></td>
									</tr>
									<tr>
										<td>Ketinggian tanaman kopi</td>
										<td>: <?= $row->ketinggian_tanaman_kopi ?></td>
									</tr>
									<tr>
										<td>Proses pasca panen</td>
										<td>: <?= $row->proses_pasca_panen ?></td>
									</tr>
								</table>
								</p>
							</div>
							<div class="card-footer">
								<button <?= ($row->bobot_stok == "0")? "disabled='true'" : "" ?> class="btn btn-primary"
										onclick="document.getElementById('beliModalForm<?= $row->id_jual ?>').style.display='block'"
										style="width:auto;">Pilih
								</button>
							</div>
						</div>
					</div>

					<!-- modal form -->
					<div id="beliModalForm<?= $row->id_jual ?>" class="modal">
						<form id="tambahKeranjang" method="post">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Beli Kopi</h5>
										<button type="button" class="close"
												onclick="document.getElementById('beliModalForm<?= $row->id_jual ?>').style.display='none'"
												data-dismiss="editModalform" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<div class="form-group">
											<label for="bobot">Bobot yang mau dibeli</label>
											<input type="number" name="bobot" class="form-control" id="bobot" placeholder="Masukkan bobot">
										</div>

										<input type="hidden" name="jenis_kopi" value="<?= $row->jenis_kopi ?>"/>
										<input type="hidden" name="id_jual" value="<?= $row->id_jual ?>"/>
										<input type="hidden" name="id_pembeli" value="<?= $id_pembeli ?>"/>

									</div>
									<div class="modal-footer">
										<!-- <button type="button" onclick="document.getElementById('modalform').style.display='none'" class="cancelbtn">Cancel</button> -->
										<button type="submit"
												onclick="document.getElementById('beliModalForm<?= $row->id_jual ?>').style.display='none'"
												class="btn btn-primary">Save
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>

				<?php endforeach; ?>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>


<?php
$this->load->view("components/footer")
?>

<!-- Bootstrap core JavaScript -->
<!-- Jquery JS-->
<script src="assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<!-- SweetAlert2 -->
<script src="<?php echo base_url("assets"); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url("assets"); ?>/plugins/toastr/toastr.min.js"></script>

<script>
	const Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 5000
	});
</script>

<script>
	// tambahLelang

	$('form[id=tambahKeranjang').submit(function (e) {
		e.preventDefault();

		$('.form-group').removeClass('has-error'); // remove the error class
		$('.help-block').remove(); // remove the error text
		$('.alert-success').remove();

		var formUrl = "<?= base_url("tambahKeranjang") ?>";

		// process the form
		$.ajax({
			type: 'POST',
			url: formUrl,
			data: new FormData(this), //penggunaan FormData
			dataType: 'json', // what type of data do we expect back from the serverss
			processData: false,
			contentType: false,
			cache: false,
			async: false,
			error: function (data) {
				alert("AJAX ERROR")
				alert(JSON.stringify(data));
			}
		})

				// using the done promise callback
				.done(function (data) {

					// here we will handle errors and validation messages
					if (!data.success) {

						Toast.fire({
							type: 'error',
							title: data.message
						});

					} else {

						// ALL GOOD! just show the success message!
						// $('form[id=form_objective]').prepend('<div class="alert alert-success">' + data.message + '</div>');
						Toast.fire({
							type: 'success',
							title: data.message
						});

						setTimeout(function () {
							window.location.href = "<?= current_url() ?>"; //will redirect to your blog page (an ex: blog.html)
						}, 1000); //will call the function after 2 secs.

					}
				});
	});
</script>

</body>

</html>
