<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GIVEN KOPI - JUAL & BELI KOPI</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/shop-homepage.css" rel="stylesheet">

    <!-- Custom styles -->
    <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

    <?php
    $this->load->view("components/member_header")
    ?>

    <!-- Page Content -->
    <div class="container">

        <!-- row -->
        <div class="row">

            <!-- col-lg-6 -->
            <div class="col-lg-6" style="margin: 50px auto">

                <form class="form-signin">
                    <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
                    <h1 class="h3 mb-3 font-weight-normal">Register</h1>
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input type="email" id="inputEmail" class="form-control mt-30" placeholder="Email address" required autofocus>
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" class="form-control mt-20" placeholder="Password" required>
                    <label for="inputRepeatPassword" class="sr-only">Repeat Password</label>
                    <input type="repeatPassword" id="inputRepeatPassword" class="form-control mt-20" placeholder="Repeat Password" required>
                    <button class="btn btn-lg btn-primary btn-block mt-30" type="submit">Register</button>
                    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
                </form>


            </div>
            <!-- /.col-lg-6 -->

        </div>
        <!-- /.row-->

    </div>
    <!-- /.Page Content -->

    <?php
    $this->load->view("components/footer")
    ?>

    <!-- Bootstrap core JavaScript -->
    <script src="assets/jquery/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>