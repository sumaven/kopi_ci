<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'PageController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# General member stuff
$route["logout"] = "MemberController/logout";

# Admin Stuff
$route["tambahKopiMentah"] = "AdminController/tambahKopiMentah";
$route["editKopiMentahAdmin"] = "AdminController/editKopiMentahAdmin";
$route["hapusKopiMentahAdmin"] = "AdminController/hapusKopiMentahAdmin";
$route["ubahStatus"] = "AdminController/ubahStatus";
$route["tambahLelang"] = "AdminController/tambahLelang";
$route["hapusLelang"] = "AdminController/hapusLelang";
$route["tambahJual"] = "AdminController/tambahJual";
$route["hapusJual"] = "AdminController/hapusJual";
$route["validasi"] = "AdminController/validasi";

# Petani Stuff
$route["jualKopiMentahAdmin"] = "PenjualController/jualKopiMentahAdmin";
$route["jualKopiMentahPetani"] = "PenjualController/jualKopiMentahPetani";

# Pembeli Stuff
$route["tambahEditBid"] = "PembeliController/tambahEditBid";
$route["beliSekarang"] = "PembeliController/tambahKeranjangLelang";
$route["tambahKeranjang"] = "PembeliController/tambahKeranjangBeli";
$route["hapusKeranjang"] = "PembeliController/hapusKeranjangBeli";
$route["bayarBeli"] = "PembeliController/bayarBeli";
$route["konfirmasiPembayaran"] = "PembeliController/konfirmasiPembayaran";

# PageControlling Stuff
$route["beli"] = "PageController/beli";
$route["jual"] = "PageController/jual";
$route["lelang"] = "PageController/lelang";
$route["lelang/(:any)"] = "PageController/lelangWithID/$1";
$route["keranjang"] = "PageController/keranjang";
$route["pembayaran"] = "PageController/pembayaran";