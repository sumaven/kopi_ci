<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JualKopiModel extends CI_Model{

    function insertJualKopiAdmin($data_pass){
        $this->db->trans_start();
        
        $data_pass["status"] = "belum-divalidasi";
        $affected_rows = $this->db->insert("transaksi_jual_kopi_admin", $data_pass);
        $inserted_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $inserted_id;
    }

    function edit($id_transaksi_jual, $data_pass){
        $this->db->where("id_transaksi_jual", $id_transaksi_jual);
        return $this->db->update("transaksi_jual_kopi_admin", $data_pass);
    }

    function getAllJualKopiAdminById($id_petani, $status){
        $this->db->join("petani", "transaksi_jual_kopi_admin.id_petani=petani.id_petani");
        $this->db->join("user", "petani.id_user=user.id_user");
        $this->db->where("transaksi_jual_kopi_admin.id_petani", $id_petani);
        $this->db->where("status", $status);
        $query = $this->db->get("transaksi_jual_kopi_admin");
        return $query;
    }

    function getAllJualKopiAdmin($status){
        $this->db->join("petani", "transaksi_jual_kopi_admin.id_petani=petani.id_petani");
        $this->db->join("user", "petani.id_user=user.id_user");
        $this->db->where("status", $status);  
        $query = $this->db->get("transaksi_jual_kopi_admin");
        return $query;
    }

    function insertJualKopiPetani($data_pass){
        $this->db->trans_start();
        
        $affected_rows = $this->db->insert("transaksi_jual_kopi_petani", $data_pass);
        $inserted_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $inserted_id;
    }

}
