<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MemberModel extends CI_Model{

    function getMember($data_pass){
        $this->db->trans_start();
        $this->db->select($data_pass["table_name"].".*".", user.id_user");
        $this->db->join("user", "user.id_user={$data_pass['table_name']}.id_user");
        $this->db->where('user.email', $data_pass["email"]);
        $this->db->where('user.password', $data_pass["password"]);
        $result = $this->db->get($data_pass["table_name"], 1);
        $this->db->trans_complete();
        return $result;
    }

    function insert($table_name, $data_pass){
        $this->db->trans_start();
        $id_user =  $this->db->insert("user", $data_pass);
        $inserted_id = $this->db->insert_id();
        $this->db->trans_complete();

        $data_pass = array(
            "id_user"  => $inserted_id,
        );

        $affectedRows = $this->db->insert($table_name, $data_pass);
        
        return $affectedRows;
    }

}
