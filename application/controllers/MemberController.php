<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MemberController extends CI_Controller
{

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
}
