<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait KeranjangController
{
    public function tambahKeranjangLelang()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$keranjang_query = $this->lelang_model->getKeranjangByLelangId($data_pass["id_lelang"]);
			$keranjang_item = $keranjang_query->row();

			if ($keranjang_item != null){
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Lelang sudah soldout!';
				echo json_encode($data);
				return;
			}

			$isSuccess = $this->lelang_model->insertKeranjang($data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Penambahan ke keranjang berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Penambahan ke keranjang gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function tambahKeranjangBeli()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_jual = $data_pass["id_jual"];
			$bobot_ambil = $data_pass["bobot"];

			$bobot_stok = $this->jualbeli_model->getJual($id_jual)->row()->bobot_stok;
			$bobot_dikurang = $bobot_stok - $bobot_ambil;

			$isSuccess = false;
			if ($bobot_dikurang >= 0){
				$this->jualbeli_model->insertKeranjang($data_pass);
				$data_pass = array(
					"bobot_stok" => $bobot_dikurang,
				);
				$this->jualbeli_model->edit($id_jual, $data_pass);
				$isSuccess = true;
			} else {
				// bobot mines atau tidak valid
				$data['message'] = "Jumlah tidak valid";
			}

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Penambahan ke keranjang berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;

				if (empty($data['message'])){
					$data['message'] = 'Penambahan ke keranjang gagal!';
				}
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function ubahKeranjangBeli()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$isSuccess = $this->jualbeli_model->insertKeranjang($data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Penambahan ke keranjang berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Penambahan ke keranjang gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function hapusKeranjangBeli()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$isSuccess = $this->jualbeli_model->hapusKeranjang($data_pass["id_keranjang_jual"]);

			$id_jual = $data_pass["id_jual"];
			$bobot = $data_pass["bobot"];
			$id_keranjang_jual = $data_pass["id_keranjang_jual"];

			unset($data_pass["id_jual"]);
			unset($data_pass["id_keranjang_jual"]);

			$data_pass = array();
			$bobot_stok = $this->jualbeli_model->getJual($id_jual)->row()->bobot_stok;

			$data_pass["bobot_stok"] = $bobot_stok + $bobot;

			$isSuccess = $this->jualbeli_model->edit($id_jual, $data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Penghapusan belanjaan di keranjang berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Penghapusan belanjaan di keranjang gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
