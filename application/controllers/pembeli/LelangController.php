<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait LelangController
{

    public function tambahEditBid()
    {
        date_default_timezone_set('Asia/Jakarta');

        $errors         = array();      // array to hold validation errors
        $data           = array();      // array to pass back data
        $data_pass         = array();

        foreach ($_POST as $key => $value) {
            $data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
        }

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $data['success'] = false;
            $data['errors']  = $errors;
        } else {

            $isSuccess = false;

            $options = array(
                "id_lelang" => $data_pass["id_lelang"],
                "id_pembeli" => $data_pass["id_pembeli"],
            );

            $id_lelang = $data_pass["id_lelang"];
            $id_pembeli = $data_pass["id_pembeli"];


            # Filter 1
            # --------------------------------------------
            $keranjang_query = $this->lelang_model->getKeranjangByLelangId($data_pass["id_lelang"]);
            $keranjang_item = $keranjang_query->row();

            if ($keranjang_item != null) {
                $data['success'] = false;
                $data['errors']  = $errors;
                $data['message'] = 'Lelang sudah soldout!';
                echo json_encode($data);
                return;
            }


            # Filter 2
            # --------------------------------------------
            $selectLelangQuery = $this->lelang_model->getLelang($data_pass["id_lelang"]);
            $lelang = $selectLelangQuery->row();

            $date = new DateTime();
            $now = $date->format("Y-m-d H:i:s");

            if ($now > $lelang->waktu_berakhir) {
                $data['success'] = false;
                $data['errors']  = $errors;
                $data['message'] = 'Waktu lelang sudah berakhir!';
                echo json_encode($data);
                return;
            }

            # Filter 3
            # --------------------------------------------
            $my_bid = $data_pass["bid"];
            $bid_query = $this->lelang_model->getMaxBid($id_lelang);
            $bid_item = $bid_query->row();

            $max_bid = $bid_item->bid;
            if ($my_bid < $max_bid){
                $data['success'] = false;
                $data['errors']  = $errors;
                $data['message'] = 'Harga bid harus lebih besar dari bid paling besar! <br> Bid paling besar sekarang : '.$max_bid."<br> Atau refresh halaman untuk melihat nilai bid terakhir";
                echo json_encode($data);
                return;
            }


            # Filter 4
            # --------------------------------------------
            $my_bid = $data_pass["bid"];
            $harga_bid_awal = $lelang->harga_bid_awal;
            $harga_bid_sekarang = $my_bid - $harga_bid_awal;
            $kelipatan_bid = $lelang->kelipatan_bid;

            if ($harga_bid_sekarang % $kelipatan_bid != 0){
                $data['success'] = false;
                $data['errors']  = $errors;
                $data['message'] = "Nilai BID berikutnya harus sesuai dengan kelipatan bid";
                echo json_encode($data);
                return;
            }


            # Last filter
            # --------------------------------------------
            $selectBidQuery = $this->lelang_model->getBid($options);
            if ($selectBidQuery->num_rows() > 0) {
                $isSuccess = $this->lelang_model->editBidLelang($id_lelang, $id_pembeli, $data_pass);

                // show a message of success and provide a true success variable
                if ($isSuccess) {
                    $data['success'] = true;
                    $data['message'] = 'Edit bid berhasil!';
                } else {
                    $data['success'] = false;
                    $data['errors']  = $errors;
                    $data['message'] = 'Edit bid gagal!';
                }
            } else {
                $isSuccess = $this->lelang_model->insertBidLelang($data_pass);

                // show a message of success and provide a true success variable
                if ($isSuccess) {
                    $data['success'] = true;
                    $data['message'] = 'Tambah bid berhasil!';
                } else {
                    $data['success'] = false;
                    $data['errors']  = $errors;
                    $data['message'] = 'Tambah bid gagal!';
                }
            }
        }

        // return all our data to an AJAX call
        echo json_encode($data);
    }
}
