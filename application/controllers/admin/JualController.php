<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait JualController
{

	public function tambahJual()
	{
		$errors = array();      // array to hold validation errors
		$data = array();      // array to pass back data
		$data_pass = array();

		$error_string = checkFile($_FILES['gambar_kopi']);
		if (!empty($error_string)) {
			$data['message'] = $error_string;
			$data['success'] = false;
			echo json_encode($data);
			return;
		}

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors'] = $errors;
		} else {

			unset($data_pass["gambar_kopi"]);
			$insertedId = $this->jualbeli_model->insert($data_pass);
			$isSuccess = $insertedId;

			if ($isSuccess) {
				# Upload gambar
				# -------------------------------------------------------------------
				$config['upload_path'] = "./uploads/";
				$config['file_name'] = "kopi_jual_" . $insertedId;
				$config['allowed_types'] = 'gif|jpg|png';
				// $config['encrypt_name'] = TRUE;

				$this->load->library('upload', $config);

				if (!$this->upload->do_upload("gambar_kopi")) {
					$data['message'] = $this->upload->display_errors();
					$data['errors'] = $errors;
					$data['success'] = false;
					echo json_encode($data);
					return;
				}
			}

			if ($isSuccess) {
				$data_image = array(
					"image_location" => "uploads/{$config['file_name']}" . $this->upload->data('file_ext'),
				);

				$isSuccess = $this->jualbeli_model->edit($insertedId, $data_image);
			}
			# -------------------------------------------------------------------

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Tambah kopi jual berhasil!';
			} else {
				$data['success'] = false;
				$data['errors'] = $errors;
				$data['message'] = 'Tambah kopi jual gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

	public function hapusJual()
	{
		$errors = array();      // array to hold validation errors
		$data = array();      // array to pass back data
		$data_pass = array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors'] = $errors;
		} else {

			$id_lelang = $data_pass["id_lelang"];
			$isSuccess = $this->jualbeli_model->hapus($id_lelang);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Hapus kopi jual berhasil!';
			} else {
				$data['success'] = false;
				$data['errors'] = $errors;
				$data['message'] = 'Hapus kopi jual gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}

}
