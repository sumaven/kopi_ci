<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait KeranjangController
{
    public function tambahKeranjang()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$isSuccess = $this->lelang_model->insertKeranjang($data_pass);

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Penambahan ke keranjang berhasillll!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Penambahan ke keranjang gagal!';
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
