<?php
defined('BASEPATH') or exit('No direct script access allowed');

trait PembayaranController
{
	public function validasi()
	{
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass 		= array();

		foreach ($_POST as $key => $value) {
			$data_pass[$key] = $value;

			if (empty($value)) {
				$errors = true;
				$data['message'] = "Mohon lengkapi data " . $key . " pada form";
				break;
			}
		}

		// if there are any errors in our errors array, return a success boolean of false
		if (!empty($errors)) {

			// if there are items in our errors array, return those errors
			$data['success'] = false;
			$data['errors']  = $errors;
		} else {

			$id_pembayaran = $data_pass["id_pembayaran"];
			$id_jual = $data_pass["id_jual"];
			$status = $data_pass["status"];

			$isSuccess = 0;

			if ($status === "valid") {
				$data_pass = array(
					"status" => "valid",
				);

				$isSuccess = $this->pembayaran_model->edit($id_pembayaran, $data_pass);
			} else if ($status === "batal") {
				$pembayaran = $this->pembayaran_model->getPembayaran($id_pembayaran)->row();
				$bobot = $pembayaran->bobot;
				$id_pembeli = $pembayaran->id_pembeli;
				$jenis_kopi = $pembayaran->jenis_kopi;

				$data_pass = array(
					"bobot" => $bobot,
					"id_jual" => $id_jual,
					"id_pembeli" => $id_pembeli,
					"jenis_kopi" => $jenis_kopi,
				);

				$isSuccess = $this->pembayaran_model->hapus($id_pembayaran);
				$isSuccess = $this->jualbeli_model->insertKeranjang($data_pass);
			}

			// show a message of success and provide a true success variable
			if ($isSuccess) {
				$data['success'] = true;
				$data['message'] = 'Validasi belanjaan berhasil!';
			} else {
				$data['success'] = false;
				$data['errors']  = $errors;
				$data['message'] = 'Validasi belanjaan gagal!' . $status;
			}
		}

		// return all our data to an AJAX call
		echo json_encode($data);
	}
}
