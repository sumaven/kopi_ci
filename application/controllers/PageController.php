<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PageController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->has_userdata("role")) {
			if ($this->session->role === "admin") {
				$this->load->view('AdminView');
			} else if ($this->session->role === "penjual") {
				$this->load->view('ViewHomePenjual');
			} else if ($this->session->role === "pembeli") {
				$this->load->view('ViewHomePembeli');
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function beli()
	{
		if ($this->session->has_userdata("role")) {
			$this->load->model('KopiMentahModel', "kopimentah_model");
			$this->load->model("JualBeliModel", "jualbeli_model");
			$this->load->model("JualKopiModel", "jualkopi_model");

			if ($this->session->role === "admin") {
				$data = array(
					"rows" => $this->kopimentah_model->getAllKopiMentahAdmin()->result(),
					"jual_kopi_belum_valid_rows" => $this->jualkopi_model->getAllJualKopiAdmin("belum-divalidasi")->result(),
					"jual_kopi_sudah_valid_rows" => $this->jualkopi_model->getAllJualKopiAdmin("sudah-divalidasi")->result(),
				);

				$this->load->view('AdminViewBeli', $data);
			} else if ($this->session->role === "pembeli") {
				$data = array(
					"rows" => $this->jualbeli_model->getAllJual()->result(),
					"id_pembeli" => $this->session->id_pembeli,
				);

				$this->load->view('ViewBeli', $data);
			} else {
				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function jual()
	{
		if ($this->session->has_userdata("role")) {

			$this->load->model('KopiMentahModel', "kopimentah_model");
			$this->load->model("JualBeliModel", "jualbeli_model");
			$this->load->model("JualKopiModel", "jualkopi_model");

			if ($this->session->role === "admin") {

				$data = array(
					"rows" => $this->jualbeli_model->getAllJual()->result(),
				);

				$this->load->view('AdminViewJual', $data);
			} else if ($this->session->role === "penjual") {

				$data = array(
					"rows" => $this->kopimentah_model->getAllKopiMentahAdmin()->result(),
					"jual_kopi_belum_valid_rows" => $this->jualkopi_model->getAllJualKopiAdminById($this->session->id_petani, "belum-divalidasi")->result(),
					"jual_kopi_sudah_valid_rows" => $this->jualkopi_model->getAllJualKopiAdminById($this->session->id_petani, "sudah-divalidasi")->result(),
				);

				$this->load->view('ViewJual', $data);
			} else {

				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function lelang()
	{
		if ($this->session->has_userdata("role")) {

			$this->load->model("LelangModel", "lelang_model");

			if ($this->session->role === "admin") {
				$data = array(
					"rows" => $this->lelang_model->getAllLelang()->result(),
				);

				$this->load->view('AdminViewLelang', $data);
			} else if ($this->session->role === "pembeli") {
				$data = array(
					"rows" => $this->lelang_model->getAllLelang()->result(),
					"id_pembeli" => $this->session->id_pembeli,
				);

				$this->load->view('ViewLelang', $data);
			} else {
				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function lelangWithID($id_lelang)
	{
		if ($this->session->has_userdata("role")) {

			$this->load->model("LelangModel", "lelang_model");

			if ($this->session->role === "admin") {
				$data = array(
					"rows" => $this->lelang_model->getPelelang($id_lelang)->result(),
				);

				$this->load->view('AdminViewPelelang', $data);
			} else {
				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function keranjang()
	{

		if ($this->session->has_userdata("role")) {

			$this->load->model("LelangModel", "lelang_model");
			$this->load->model("JualBeliModel", "jualbeli_model");
			$this->load->model("PembayaranModel", "pembayaran_model");

			if ($this->session->role === "pembeli") {
				$data = array(
					"beli_rows" => $this->jualbeli_model->getKeranjang($this->session->id_pembeli)->result(),
					"lelang_rows" => $this->lelang_model->getKeranjang($this->session->id_pembeli)->result(),
					"id_pembeli" => $this->session->id_pembeli,
				);

				$this->load->view('ViewKeranjang', $data);
			} else {
				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}

	public function pembayaran()
	{

		if ($this->session->has_userdata("role")) {

			$this->load->model("JualBeliModel", "jualbeli_model");
			$this->load->model("PembayaranModel", "pembayaran_model");

			if ($this->session->role === "pembeli") {
				$data = array(
					"rows" => $this->pembayaran_model->getPembayaranPembeli($this->session->id_pembeli)->result(),
					"id_pembeli" => $this->session->id_pembeli,
				);

				$this->load->view('ViewPembayaran', $data);
			} else if ($this->session->role === "admin"){
				$data = array(
					"rows" => $this->pembayaran_model->getAllPembayaran()->result(),
				);

				$this->load->view('AdminViewPembayaran', $data);

			} else {
				header("refresh:0; url=" . base_url());
			}
		} else {
			$this->load->view('ViewHome');
		}
	}
}
